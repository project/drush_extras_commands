<?php
/**
 * Created by PhpStorm.
 * User: mdurand
 * Date: 02/05/19
 * Time: 16:08
 */

namespace Drupal\drush_extras_commands\Commands;



use Drupal\drush_extras_commands\Helpers\NodeHelper;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

class NodeCommands extends DrushCommands {

  private $nodeHelper;

  /**
   * Constructs a new EntityCommands object.
   *
   * @param \Drupal\drush_extras_commands\Commands\NodeHelper $nodeHelper
   */
  public function __construct(NodeHelper $nodeHelper) {
    $this->nodeHelper= $nodeHelper;
  }

  /**
   * Unpublish all nodes by optional conditons.
   *
   * @param string $type
   * @param array  $options
   *
   * @option  conditions
   *    JSON Conditions for the query.
   *
   * @command drush-extras:node:unpublish
   * @aliases drex-noun
   *
   * @usage   drex-enrm drex-noun --conditions='{"type":"projet"}'
   *     conditions JSON is the conditions of the query
   * @throws \Drush\Exceptions\UserAbortException
   */
  public function unpublishNode($type = '',$options = ['conditions' => NULL]){
    $conditions = json_decode($options['conditions'], TRUE);
    $conditions['status'] = 1;
    if ($type != ''){
      $conditions['type']=$type;
    }

    $nids = $this->nodeHelper->getEntitiesIdByConditions('node', $conditions);
    if (!$this->io()->confirm(dt(count($nids) . ' nodes will be unpublished. Do you want to continue ?'))) {
      throw new UserAbortException();
    }

    $this->nodeHelper->unpublishNode($nids);
  }

}

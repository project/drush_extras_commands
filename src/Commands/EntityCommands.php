<?php

namespace Drupal\drush_extras_commands\Commands;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\drush_extras_commands\Helpers\EntityHelper;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Class EntityCommands.
 */
class EntityCommands extends DrushCommands {

  private $entityHelper;

  /**
   * Constructs a new EntityCommands object.
   *
   * @param \Drupal\drush_extras_commands\Helpers\EntityHelper $entityHelper
   */
  public function __construct(EntityHelper $entityHelper) {
    $this->entityHelper= $entityHelper;
  }

  /**
   * Remove all entity by optional limit and conditions.
   *
   * @param array $options
   * @param string $entityType
   *    The entity type (node is the default value).
   *
   * @option  limit
   *    Return the first X results of the query.
   * @option  conditions
   *    JSON Conditions for the query.
   *
   * @command drush-extras:entity:remove
   * @aliases drex-enrm
   *
   * @usage   drex-enrm node --conditions='{"type":"projet"}' --limit=20
   *     node is the type of entity, conditions JSON is the conditions of the
   *     query, limit option is the number of row concerned
   *
   * @throws \Drush\Exceptions\UserAbortException
   */
  public function entityRemove($entityType, $options = ['limit' => 0, 'conditions' => NULL]) {
    $limit = $options['limit'];
    $conditions = json_decode($options['conditions'], TRUE);
    $operations = [];
    $numOperations = 0;
    $batchId = 1;

    $eids = $this->entityHelper->getEntitiesIdByConditions($entityType, $conditions, $limit);

    if (!$this->io()->confirm(dt(count($eids) . ' ' . $entityType . ' will be deleted. Do you want to continue ?'))) {
      throw new UserAbortException();
    }

    if (!empty($eids)){
        foreach ($eids as $eid){
          $this->io()->writeln('Preparing batch: '.$batchId);
          $operations[]=[
              '\Drupal\drush_extras_commands\Controller\EntityBatchController::deleteEntity',
              [
                $eid,
                $entityType,
                $batchId,
                t('Deleting entity @eid', ['@eid' => $eid]),
              ]
          ];
          $batchId++;
          $numOperations++;
        }
    } else {
      $this->io->warning('No entity of this type');;
    }

    $batch = [
      'title' => t('Deleting @num entitie(s)', ['@num' => $numOperations]),
      'operations' => $operations,
      'finished' => '\Drupal\drush_extras_commands\Controller\EntityBatchController::deleteEntityFinished',
    ];

    batch_set($batch);
    drush_backend_batch_process();
    $this->io()->success('Successfully removed.');
  }
}

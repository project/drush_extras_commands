<?php

namespace Drupal\drush_extras_commands\Helpers;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Created by PhpStorm.
 * User: mdurand
 * Date: 25/04/19
 * Time: 11:53
 */

class EntityHelper {

  protected $entityTypeManager;


  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  private function getStorageByEntityType($entityType){
    try {
      $storage =  $this->entityTypeManager->getStorage($entityType);
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }

    return $storage;
  }

  public function getEntitiesIdByConditions($entityType, $conditions, $limit = 0){
    $storage = $this->getStorageByEntityType($entityType);
    $query = $storage->getQuery();

    if ($conditions) {
      foreach ($conditions as $key => $value) {
        $query->condition($key, $value);
      }
    }
    if ($limit != 0) {
      $query->range(0, $limit);
    }

    $eids = $query->execute();
    return $eids;
  }

  public function deleteEntities($eids, $entityType){
    foreach ($eids as $eid) {
      $this->deleteEntity($eid, $entityType);
    }
  }

  public function deleteEntity($eid, $entityType) {
    $storage = $this->getStorageByEntityType($entityType);
    $entity = $storage->load($eid);
    try {
      $entity->delete();
    } catch (EntityStorageException $e) {
      dump($e);
    }
  }

  public function getEntities($entityType,$eids)
  {
    $storage = $this->getStorageByEntityType($entityType);
    return $storage->loadMultiple($eids);
  }

}

<?php
/**
 * Created by PhpStorm.
 * User: mdurand
 * Date: 03/05/19
 * Time: 10:33
 */

namespace Drupal\drush_extras_commands\Helpers;


class NodeHelper extends EntityHelper {

  public function unpublishNode($nids){
    $nodes = $this->getEntities('node', $nids);
    foreach ($nodes as $node){
      foreach (array_keys($node->getTranslationLanguages()) as $translation){
        $nodeTranslate = $node->getTranslation($translation);
        $nodeTranslate->setPublished(FALSE);
        $nodeTranslate->save();
      }
    }
  }

}

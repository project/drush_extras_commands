<?php
/**
 * Created by PhpStorm.
 * User: mdurand
 * Date: 24/04/19
 * Time: 16:49
 */

namespace Drupal\drush_extras_commands\Controller;

use Drupal;

class EntityBatchController {


  public function deleteEntity($eid, $entityType, $batchId ,$operationDetails, &$context){

    $entityHelper = Drupal::service('drush_extras_commands.entity_helper');
    $entityHelper->deleteEntity($eid, $entityType);

    $context['result'][] = $batchId;
    $context['message']= t('Running Batch "@id" @details', ['@id' => $batchId, '@details' => $operationDetails]);
  }

  public function deleteEntityFinished ($success, array $results, array $operations){
    $messenger = Drupal::messenger();

    if ($success){
      $messenger->addMessage(t('@count results processed.', ['@count' => count($results)]));
    } else {
      $errorOperation = reset($operations);
      $messenger->addMessage(t('An error occurred while processing @operation with arguments : @args',
        [
          '@operation' => $errorOperation[0],
          '@args' => print_r($errorOperation[0], TRUE),
        ]));
    }
  }

}
